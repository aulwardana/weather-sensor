#include <SPI.h>
#include <LoRa.h>

#include <MQ135.h>
#include <MQ7.h>

#include <ArduinoJson.h>

//Wind speed variable declaration
int windSensorValue;
float outvoltageWind;
int Level;

//Wind direction variable declaration
#define Offset 0;
int windDirectSensorValue;
int Direction;
int CalDirection;
int LastValue; 

//Air quality variable declaration
float airQualitySensorValue = 0;

//CO measurement variable declaration
float coSensorValue = 0;
float ISPUCoValue = 0;

//Dust measurement variable declaration
int ledPower = 12; 
int samplingTime = 280;
int deltaTime = 40;
int sleepTime = 9680;
float voMeasured = 0;
float calcVoltage = 0;
float dustDensity = 0;

//JSON data format
StaticJsonBuffer<800> jsonBufferIdentity;
JsonObject& sensing = jsonBufferIdentity.createObject();

void setup() {
  Serial.begin(9600);
  pinMode(ledPower,OUTPUT);

  if (!LoRa.begin(9150E5)) {

    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

int readWindSpeedData(){
  //Read sensor value on A0
  windSensorValue = analogRead(A0);

  //Convert sensor value on A0
  outvoltageWind = windSensorValue * (5.0 / 1023.0);
  //Serial.print("outvoltage wind sensor = ");
  //Serial.print(outvoltageWind);
  //Serial.println("V");

  //The level of wind speed is proportional to the output voltage.
  Level = 6*outvoltageWind;
  //Serial.print("wind speed is ");
  //Serial.print(Level);
  //Serial.println(" level now");
  //Serial.println();
  return Level;
  
  delay(500);
}

String readWindDirectData(){
  //Read sensor value on A1
  windDirectSensorValue = analogRead(A1);
  //Serial.print("analog wind direction sensor = ");
  //Serial.print(windDirectSensorValue); 

  //Mapping analog value to direction degree
  Direction = map(windDirectSensorValue, 0, 1023, 0, 360); 
  CalDirection = Direction + Offset; 

  //Normalization direction
  if(CalDirection > 360) 
  CalDirection = CalDirection - 360; 
  
  if(CalDirection < 0) 
  CalDirection = CalDirection + 360; 
  
  // Only update the display if change greater than 2 degrees. 
  if(abs(CalDirection - LastValue) > 5) { 
    //Serial.print("direction value = "); 
    //Serial.print(CalDirection); 
    if(CalDirection < 22) 
    //Serial.println("N");
    return "N"; 
    else if (CalDirection < 67) 
    //Serial.println("NE");
    return "NE"; 
    else if (CalDirection < 112) 
    //Serial.println("E"); 
    return "E";
    else if (CalDirection < 157) 
    //Serial.println("SE"); 
    return "SE";
    else if (CalDirection < 212) 
    //Serial.println("S");
    return "S"; 
    else if (CalDirection < 247) 
    //Serial.println("SW");
    return "SW"; 
    else if (CalDirection < 292) 
    //Serial.println("W");
    return "W"; 
    else if (CalDirection < 337) 
    //Serial.println("NW");
    return "NW"; 
    else 
    //Serial.println("N");
    return "N";  
    LastValue = CalDirection; 
  } 

  delay(500);
}

float readAirQualityData(){
  //Read sensor value on A2
  MQ135 gasSensor = MQ135(A2);

  //Normalization sensor value
  for (int i = 0; i <= 1000; i++) {
    airQualitySensorValue = gasSensor.getPPM()+airQualitySensorValue;
  }
  airQualitySensorValue = airQualitySensorValue/1000;

  //Serial.print("CO2 : "); 
  //Serial.print(airQualitySensorValue);
  //Serial.print(" ppm");

  return airQualitySensorValue;

  delay(500);
}

float readCoData(){
  //Read sensor value on A3
  MQ7 coSensor(A3,5.0);

  //Normalization sensor value
  for (int i = 0; i <= 1000; i++)
      {
        coSensorValue = coSensor.getPPM()+coSensorValue;
      }
  coSensorValue = coSensorValue/1000;

  //Mapping sensor value
  coSensorValue = map(coSensorValue, 0, 700, 0, 1400);
  coSensorValue = (coSensorValue / 24.57143);
  //Serial.print("CO : ");
  //Serial.print(coSensorValue);
  //Serial.print(" ug/m3 ");

  return coSensorValue;

  delay(500);
}

float readISPUCoValue(){

  //Reading CO sensor value 
  float coSensorValueRead = readCoData();
  
  if (coSensorValueRead <= 5)
  {
    ISPUCoValue = 10 * (coSensorValueRead - 0) + 0;
  }
  else if (coSensorValueRead <= 10)
  {
    ISPUCoValue = 10 * (coSensorValueRead - 5) + 50;
  }
  
  else if (coSensorValueRead <= 17)
  {
    ISPUCoValue = 14.285714286 * (coSensorValueRead - 10) + 100;
  }
  
  else if (coSensorValueRead <= 34)
  {
    ISPUCoValue = 5.882352941 * (coSensorValueRead - 17) + 200;
  }
  
  else if (coSensorValueRead <= 46)
  {
    ISPUCoValue = 8.333333333 * (coSensorValueRead - 34) + 300;
  }
  
  else if (coSensorValueRead <= 57.5)
  {
    ISPUCoValue = 8,695652174 * (coSensorValueRead - 46) + 400;
  }

  //Serial.print(" - nilai ISPU CO : ");
  //Serial.print(ISPUCoValue);

  return ISPUCoValue; 

  delay(500);
}

float readPMData(){
  digitalWrite(ledPower,LOW);
  delayMicroseconds(samplingTime);

  //Read sensor value on A4
  voMeasured = analogRead(A4);
 
  delayMicroseconds(deltaTime);
  digitalWrite(ledPower,HIGH);
  delayMicroseconds(sleepTime);
 
  // 0 - 3.3V mapped to 0 - 1023 integer values
  // recover voltage
  calcVoltage = voMeasured * (3.3 / 1024);
  dustDensity = 0.17 * calcVoltage - 0.1;
 
  //Serial.print("Raw Signal Value (0-1023): ");
  //Serial.print(voMeasured);
  //Serial.print(" - Voltage: ");
  //Serial.print(calcVoltage);
  //Serial.print(" - Dust Density: ");
  //Serial.println(dustDensity);

  return dustDensity; 
  delay(500);
}

void loraSend(uint8_t datasend[72]){
  LoRa.beginPacket();
  LoRa.print((char*)datasend);
  LoRa.endPacket();
  delay(5000);
}

void loop() {
  int var1 = readWindSpeedData();
  String var2 = readWindDirectData();
  float var3 = readAirQualityData();
  float var4 = readCoData();
  float var5 = readISPUCoValue();
  float var6 = readPMData();

  sensing["deviceID"] = "device01";
  sensing["wind"] = var1;
  sensing["direction"] = var2;
  sensing["air"] = var3; 
  sensing["co"] = var4; 
  sensing["ispu"] = var5;
  sensing["dust"] = var6;

  char JSONsensingBuffer[1000];
  sensing.printTo(JSONsensingBuffer, sizeof(JSONsensingBuffer));

  loraSend(JSONsensingBuffer);
}
