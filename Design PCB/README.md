# PCB Design 

### Scheme Design 
![Scheme Design](https://gitlab.com/wianoski/weather-sensor/raw/master/Design%20PCB/scheme.png)

### Board Design v1
![Board Design](https://gitlab.com/wianoski/weather-sensor/raw/master/Design%20PCB/v1.png) 

### Board Design v2
![Board Design](https://gitlab.com/wianoski/weather-sensor/raw/master/Design%20PCB/v2.png)
